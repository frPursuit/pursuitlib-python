from enum import Enum


class IndexedEnum(Enum):
    """
    An Java-like enumeration where each entry is identified by a numeric index
    """
    @classmethod
    def values(cls):
        """
        Get a list of all the values that are part of this enum
        :return: A list of all of this enum's values
        """
        return list(cls)

    def __init__(self, *args):
        self.ordinal = len(type(self))

    def __str__(self) -> str:
        return self.name

    def __int__(self) -> int:
        return self.ordinal
