"""
This module contains helpful methods that can be used to raise exceptions
"""

from typing import Callable


def not_implemented(obj: object, method: Callable) -> NotImplementedError:
    """
    Get an exception caused by the non-implementation of a method for a particular class
    :param obj: The object the method is called on
    :param method: The method that is not implemented
    :return: A NonImplementedError explaining that the provided method is not implemented for the provided class
    """
    return NotImplementedError(f"Method '{method.__name__}' is not implemented for '{type(obj).__name__}'")
