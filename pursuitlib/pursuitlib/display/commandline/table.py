from typing import List, Tuple

from pursuitlib.display.commandline.text.textcomponent import TextComponent


class Table:
    """
    A table that can be written in the console
    """

    # The padding to use between columns
    COLUMN_PADDING = 1

    def __init__(self):
        """
        Create a new Table
        """
        self.rows: List[Tuple[any, ...]] = []

    def append_row(self, *columns: any):
        """
        Append a row to the table
        :param columns: A tuple that contains all of columns in that row
        """
        self.rows.append(columns)

    def get_row_count(self) -> int:
        """
        Get the number of rows in the table
        :return: The table's row count
        """
        return len(self.rows)

    def get_column_count(self) -> int:
        """
        Get the number of columns in the table
        :return: The table's column count
        """
        if len(self.rows) == 0:
            return 0
        return max([len(row) for row in self.rows])

    def get_column_widths(self) -> List[int]:
        """
        Get the widths of all the columns in the table
        :return: The table's column widths, in characters
        """
        widths = [0] * self.get_column_count()
        for row in self.rows:
            for col, component in enumerate(row):
                widths[col] = max(widths[col], len(component))
        return widths

    def as_lines(self) -> List[TextComponent]:
        """
        Transform this table to a list of TextComponents
        :return: A list of TextComponents that represents this table
        """
        lines = []
        widths = self.get_column_widths()
        for row in self.rows:
            line: List[any] = []
            for col, component in enumerate(row):
                padding = widths[col] - len(component)
                if col < len(widths) - 1:  # Do not add column padding after the last column
                    padding += Table.COLUMN_PADDING
                line.append(component)
                line.append(' ' * padding)
            lines.append(TextComponent(*line))
        return lines

    def as_text(self) -> TextComponent:
        """
        Transform this table into a single TextComponent
        :return: A TextComponent that represents this table
        """
        components: List[any] = []
        lines = self.as_lines()
        for i, line in enumerate(lines):
            components.append(line)
            # Do not append a newline at the end of the table
            if i < len(lines) - 1:
                components.append('\n')
        return TextComponent(*components)

    def __str__(self) -> str:
        return str(self.as_text())
