from typing import Optional

from pursuitlib.display.commandline.text import textformatting, textstyle
from pursuitlib.display.commandline.text.textstyle import TextStyle, TextStyleRef


class TextComponent:
    """
    A stylized text component that can be written to the console
    """
    def __init__(self, *children: any, style: Optional[TextStyleRef] = None):
        """
        Create a new text component with the specified style, composed of
        the provided children.

        If this text component contains child text components, the child styles
        will be applied to their respective text
        :param children: The children of this text component
        :param style: The style of this text component
        """
        self._style = TextStyle.from_ref(style or TextStyle())
        self._children = children

    @property
    def style(self) -> TextStyle:
        """
        The style of this text component
        """
        return self._style

    @property
    def children(self) -> tuple:
        """
        The children of this text component
        """
        return self._children

    def render(self, formatted: bool = True) -> str:
        """
        Render this text component as a string
        :param formatted: Whether this component should be \
                          formatted or rendered as raw text
        :return: The rendered text component, as a string
        """
        rendered = self._render_component(textstyle.DEFAULT, formatted)
        if formatted:
            # Ensure this component isn't affected by the previous formatting,
            # and doesn't affect the following formatting
            return f"{textformatting.RESET}{rendered}{textformatting.RESET}"
        else:
            return rendered

    def _render_component(self, parent_style: TextStyle, formatted: bool) -> str:
        rendered = ""
        # Apply this style if rendering formatted text
        if formatted:
            rendered += str(self.style.get_formatting(parent_style))
        style = self.style.apply_to(parent_style) if formatted else parent_style
        for child in self.children:
            if isinstance(child, TextComponent):
                rendered += child._render_component(style, formatted)
                # Restore this component's style if rendering formatted text
                if formatted:
                    rendered += str(style.get_formatting(child.style, True))
            else:
                rendered += str(child)
        return rendered

    def __str__(self) -> str:
        return self.render(False)

    def __add__(self, addition: any) -> "TextComponent":
        return TextComponent(self, addition)
