from typing import Tuple

from pursuitlib.display.commandline.vtsequence import VTSequence


class TextFormatting:
    """
    Formatting codes that can be used to format text
    """

    @staticmethod
    def combine(*formattings: "TextFormatting") -> "TextFormatting":
        """
        Combine a set of formatting codes into a single TextFormatting instance
        :param formattings: The formatting codes to combine
        :return: A combined TextFormatting instance
        """
        # Optimization: if no formattings are provided, return an empty text formatting
        if len(formattings) == 0:
            return TextFormatting()
        # Optimization: if only 1 text formatting is provided, return it
        if len(formattings) == 1:
            return formattings[0]

        codes = []
        for formatting in formattings:
            codes += formatting.codes
        return TextFormatting(*codes)

    def __init__(self, *codes: int):
        """
        Create a new text formatting sequence
        composed of the specified codes
        :param codes: The codes that are used to format text
        """
        self._codes = codes

    @property
    def codes(self) -> Tuple[int, ...]:
        """
        Codes that are used to format text
        """
        return self._codes

    def to_sequence(self) -> VTSequence:
        """
        Get the VT sequence that corresponds to this text formatting
        :return: The VT sequence that corresponds to this text formatting
        """
        # If this text formatting is empty, return an empty VT sequence
        if len(self._codes) == 0:
            return VTSequence("")
        codes = ';'.join([str(c) for c in self._codes])
        return VTSequence(f"\x1b[{codes}m")

    def __str__(self) -> str:
        return str(self.to_sequence())

    def __eq__(self, other: any) -> bool:
        if not isinstance(other, TextFormatting):
            return False
        return self._codes == other._codes

    def __hash__(self) -> int:
        return hash(self._codes)


# The text formatting sequence used to reset the text format to its default
RESET = TextFormatting(0)
