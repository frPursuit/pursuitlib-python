from pursuitlib.display.color import Color
from pursuitlib.display.commandline.text.textformatting import TextFormatting


class TextColor:
    """
    A color that can be applied to a
    text foreground or background
    """

    # The default color type
    _COLOR_DEFAULT = 0x00

    # The 4-bit standard color type
    _COLOR_4 = 0x01

    # The 8-bit standard color type
    _COLOR_8 = 0x02

    # The 24-bit RGB color type
    _COLOR_RGB = 0x03

    @staticmethod
    def standard4(code: int):
        """
        Create a 4-bit standard color.

        More info on 4-bit colors at: https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit
        :param code: The code of the standard color
        :return: A TextColor instance that represents the standard color
        """
        return TextColor(TextColor._COLOR_4, code, 0, 0)

    @staticmethod
    def standard8(code: int):
        """
        Create a 8-bit standard color.

        More info on 8-bit colors at: https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
        :param code: The code of the standard color
        :return: A TextColor instance that represents the standard color
        """
        return TextColor(TextColor._COLOR_8, code, 0, 0)

    @staticmethod
    def rgb(color: Color) -> "TextColor":
        """
        Create a TextColor instance from an RGB color
        :param color: The color to create the TextColor instance from
        :return: A TextColor instance that represents the provided RGB color
        """
        return TextColor(TextColor._COLOR_RGB, color.r, color.g, color.b)

    def __init__(self, color_type: int, r: int, g: int, b: int):
        """
        Create a new text color
        :param color_type: The type of the color
        :param r: The color, or the red component if this is a RGB color
        :param g: The green component if this is a RGB color
        :param b: The blue component if this is a RGB color
        """
        self._type = color_type
        self._r = r
        self._g = g
        self._b = b

    def to_foreground(self) -> TextFormatting:
        """
        Get the TextFormatting instance used to set
        the text's foreground color to this color
        :return: The TextFormatting instance used to \
                 set the text's foreground color
        """
        if self._type == TextColor._COLOR_DEFAULT:
            return TextFormatting(39)
        elif self._type == TextColor._COLOR_4:
            return TextFormatting(self._r)
        elif self._type == TextColor._COLOR_8:
            return TextFormatting(38, 5, self._r)
        elif self._type == TextColor._COLOR_RGB:
            return TextFormatting(38, 2, self._r, self._g, self._b)
        else:
            raise self._invalid_color_type()

    def to_background(self) -> TextFormatting:
        """
        Get the TextFormatting instance used to set
        the text's background color to this color
        :return: The TextFormatting instance used to \
                 set the text's background color
        """
        if self._type == TextColor._COLOR_DEFAULT:
            return TextFormatting(49)
        elif self._type == TextColor._COLOR_4:
            return TextFormatting(self._r + 10)
        elif self._type == TextColor._COLOR_8:
            return TextFormatting(48, 5, self._r)
        elif self._type == TextColor._COLOR_RGB:
            return TextFormatting(48, 2, self._r, self._g, self._b)
        else:
            raise self._invalid_color_type()

    def __eq__(self, other: any) -> bool:
        if not isinstance(other, TextColor):
            return False
        return ((self._type, self._r, self._g, self._b) ==
                (other._type, other._r, other._g, other._b))

    def __hash__(self) -> int:
        return hash((self._type, self._r, self._g, self._b))

    def _invalid_color_type(self) -> ValueError:
        return ValueError(f"Invalid TextColor type: 0x{self._type:x}")


# The default text color
DEFAULT = TextColor(0, 0, 0, 0)


# Standard terminal text colors
BLACK = TextColor.standard4(30)
DARK_RED = TextColor.standard4(31)
DARK_GREEN = TextColor.standard4(32)
DARK_YELLOW = TextColor.standard4(33)
DARK_BLUE = TextColor.standard4(34)
DARK_MAGENTA = TextColor.standard4(35)
DARK_CYAN = TextColor.standard4(36)
LIGHT_GRAY = TextColor.standard4(37)
DARK_GRAY = TextColor.standard4(90)
LIGHT_RED = TextColor.standard4(91)
LIGHT_GREEN = TextColor.standard4(92)
LIGHT_YELLOW = TextColor.standard4(93)
LIGHT_BLUE = TextColor.standard4(94)
LIGHT_MAGENTA = TextColor.standard4(95)
LIGHT_CYAN = TextColor.standard4(96)
WHITE = TextColor.standard4(97)
