from typing import Optional, Union

from pursuitlib.display.commandline.text import textcolor
from pursuitlib.display.commandline.text.textcolor import TextColor
from pursuitlib.display.commandline.text.textformatting import TextFormatting

# Utility type used to refer to references to text styles
TextStyleRef = Union["TextStyle", TextColor]


class TextStyle:
    """
    A style that can be applied to text written to a console
    """

    _FORMAT_BOLD = TextFormatting(1)
    _FORMAT_NO_BOLD = TextFormatting(22)

    _FORMAT_UNDERLINE = TextFormatting(4)
    _FORMAT_NO_UNDERLINE = TextFormatting(24)

    _FORMAT_STRIKETHROUGH = TextFormatting(9)
    _FORMAT_NO_STRIKETHROUGH = TextFormatting(29)

    @staticmethod
    def from_ref(value: TextStyleRef) -> "TextStyle":
        """
        Return a text style from either a text style, or a text color.
        If a text color is provided, it is used as a foreground color
        :param value: The value to convert to a TextStyle
        :return: The TextStyle created from the provided value
        """
        if isinstance(value, TextStyle):
            return value
        elif isinstance(value, TextColor):
            # Use the provided color as a foreground color
            return TextStyle(foreground=value)
        else:
            raise ValueError(f"Cannot convert {type(value).__name__} to TextStyle")

    def __init__(self, foreground: Optional[TextColor] = None,
                 background: Optional[TextColor] = None,
                 bold: Optional[bool] = None,
                 underline: Optional[bool] = None,
                 strikethrough: Optional[bool] = None):
        """
        Create a new text style using the specified parameters
        :param foreground: The text's foreground color
        :param background: The text's background color
        :param bold: Whether the text is bold
        :param underline: Whether the text is underlined
        :param strikethrough: Whether the text is crossed out
        """
        self._foreground = foreground
        self._background = background
        self._bold = bold
        self._underline = underline
        self._strikethrough = strikethrough

    @property
    def foreground(self) -> Optional[TextColor]:
        """
        The text's foreground color
        """
        return self._foreground

    @property
    def background(self) -> Optional[TextColor]:
        """
        The text's background color
        """
        return self._background

    @property
    def bold(self) -> Optional[bool]:
        """
        Whether the text is bold
        """
        return self._bold

    @property
    def underline(self) -> Optional[bool]:
        """
        Whether the text is underlined
        """
        return self._underline

    @property
    def strikethrough(self) -> Optional[bool]:
        """
        Whether the text is crossed out
        """
        return self._strikethrough

    def apply_to(self, previous: "TextStyle") -> "TextStyle":
        """
        Get the effective text style obtained by applying this style
        to the provided previous style
        :param previous: The style that was previously applied
        :return: The effective style obtained after applying this style
        """
        return TextStyle(
            self.foreground or previous.foreground,
            self.background or previous._background,
            self.bold or previous.bold,
            self.underline or previous.underline,
            self.strikethrough or previous.strikethrough
        )

    def get_formatting(self, from_style: Optional["TextStyle"], revert: bool = False) -> TextFormatting:
        """
        Get a text formatting sequence that can be used to apply this style
        :param from_style: The style that was previously applied
        :param revert: This should be set to "true" when this style was applied \
                       before the provided "from_style" style, and the caller needs \
                       a formatting sequence to revert to this style
        :return: A text formatting sequence used to apply this style
        """
        formattings = (
            self._get_foreground_formatting(from_style, revert),
            self._get_background_formatting(from_style, revert),
            self._get_bold_formatting(from_style, revert),
            self._get_underline_formatting(from_style, revert),
            self._get_strikethrough_formatting(from_style, revert),
        )

        non_null = tuple(filter(lambda f: f is not None, formattings))
        return TextFormatting.combine(*non_null)

    def __eq__(self, other: any) -> bool:
        if not isinstance(other, TextStyle):
            return False
        return ((self.foreground, self.background, self.bold,
                 self.underline, self.strikethrough) ==
                (other.foreground, other.background, other.bold,
                 other.underline, other.strikethrough))

    def __hash__(self) -> int:
        return hash((self.foreground, self.background, self.bold,
                     self.underline, self.strikethrough))

    def _get_foreground_formatting(self, from_style: "TextStyle", revert: bool):
        if self.foreground is None or self.foreground == from_style.foreground:
            return None  # If the color doesn't need to change, do nothing
        if revert and from_style.foreground is None:
            return None  # If we're reverting from no change, do nothing
        if self.foreground is None:  # If the state is not specified, do nothing or revert to the default
            return textcolor.DEFAULT.to_foreground() if revert else None
        return self.foreground.to_foreground()

    def _get_background_formatting(self, from_style: "TextStyle", revert: bool):
        if self.background == from_style.background:
            return None  # If the color doesn't need to change, do nothing
        if revert and from_style.background is None:
            return None  # If we're reverting from no change, do nothing
        if self.background is None:  # If the state is not specified, do nothing or revert to the default
            return textcolor.DEFAULT.to_background() if revert else None
        return self.background.to_background()

    def _get_bold_formatting(self, from_style: "TextStyle", revert: bool):
        if self.bold == from_style.bold:
            return None  # If the state doesn't need to change, do nothing
        if revert and from_style.bold is None:
            return None  # If we're reverting from no change, do nothing
        if self.bold is None:  # If the state is not specified, do nothing or revert to the default
            return TextStyle._FORMAT_NO_BOLD if revert else None
        return TextStyle._FORMAT_BOLD if self.bold else TextStyle._FORMAT_NO_BOLD

    def _get_underline_formatting(self, from_style: "TextStyle", revert: bool):
        if self.underline == from_style.underline:
            return None  # If the state doesn't need to change, do nothing
        if revert and from_style.underline is None:
            return None  # If we're reverting from no change, do nothing
        if self.underline is None:  # If the state is not specified, do nothing or revert to the default
            return TextStyle._FORMAT_NO_UNDERLINE if revert else None
        return TextStyle._FORMAT_UNDERLINE if self.underline else TextStyle._FORMAT_NO_UNDERLINE

    def _get_strikethrough_formatting(self, from_style: "TextStyle", revert: bool):
        if self.strikethrough == from_style.strikethrough:
            return None  # If the state doesn't need to change, do nothing
        if revert and from_style.strikethrough is None:
            return None  # If we're reverting from no change, do nothing
        if self.strikethrough is None:  # If the state is not specified, do nothing or revert to the default
            return TextStyle._FORMAT_NO_STRIKETHROUGH if revert else None
        return TextStyle._FORMAT_STRIKETHROUGH if self.strikethrough else TextStyle._FORMAT_NO_STRIKETHROUGH


# The default style of text written to the console
DEFAULT = TextStyle(textcolor.DEFAULT, textcolor.DEFAULT, False, False, False)
