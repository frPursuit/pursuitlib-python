"""
This module contains functions that can be used to write stylized objects to a text output
"""

import sys
from typing import Optional, Union, IO

from pursuitlib.display.commandline.text import textcolor
from pursuitlib.display.commandline.text.textcomponent import TextComponent
from pursuitlib.display.commandline.text.textstyle import TextStyle, TextStyleRef


def write(item: any, style: Optional[TextStyleRef] = None):
    """
    Write the provided item using the provided style
    :param item: The item to write
    :param style: The style to use when writing the item
    """
    write_to(sys.stdout, item, style)


def write_line(item: any = "", style: Optional[TextStyleRef] = None):
    """
    Write the provided item using the provided style, followed by a newline
    :param item: The item to write
    :param style: The style to use when writing the item
    """
    write(_to_line(item), style)


def write_err(item: any, style: Optional[TextStyleRef] = textcolor.LIGHT_RED):
    """
    Write an error using the provided style.
    If no style is specified, the LIGHT_RED color is used
    :param item: The error to write
    :param style: The style to use when writing the item
    """
    write_to(sys.stderr, item, style)


def write_err_line(item: any = "", style: Optional[TextStyleRef] = None):
    """
    Write an error using the provided style, followed by a newline.
    If no style is specified, the LIGHT_RED color is used
    :param item: The error to write
    :param style: The style to use when writing the item
    """
    write_err(_to_line(item), style)


def write_to(stream: IO, item: any, style: Optional[TextStyleRef] = None):
    """
    Write the provided item to the provided stream using the provided style
    :param stream: The stream to write the item to
    :param item: The item to write
    :param style: The style to use when writing the item
    """
    # Allow the style to be specified as argument
    if style is not None:
        style = TextStyle.from_ref(style)
        item = TextComponent(item, style=style)

    if isinstance(item, TextComponent):
        # Only format text if the stream is a tty
        formatted = stream.isatty()
        stream.write(item.render(formatted))
    else:
        # If the item isn't a text component, write it as a string
        # without writing any formatting codes
        stream.write(str(item))


def confirm(prompt: str, default_answer: Optional[bool] = None) -> bool:
    """
    Write a confirmation prompt
    :param prompt: The question to ask in the prompt
    :param default_answer: The question's default answer
    :return: True if the answer to the question is "Yes", false if it is "No"
    """
    while True:
        if default_answer is None:
            choices = "[y/n]"
        elif default_answer:
            choices = "[Y/n]"
        else:  # not default_answer
            choices = "[y/N]"

        result = input(f"{prompt} {choices} ").strip().lower()

        if result == "" and default_answer is not None:
            return default_answer
        elif result == "y":
            return True
        elif result == "n":
            return False


def _to_line(item: any) -> Union[str, TextComponent]:
    # Make sure '\n' can be added to the item
    if not isinstance(item, TextComponent):
        item = str(item)
    return item + '\n'
