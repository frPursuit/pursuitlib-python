class VTSequence:
    """
    Virtual Terminal sequence used to send a command
    to the console
    """

    @staticmethod
    def cursor_position(x: int, y: int) -> "VTSequence":
        """
        Create a VT sequence that moves the console's
        cursor to the specified position
        :param x: The X coordinate of the cursor, starting at 0
        :param y: The Y coordinate of the cursor, starting at 0
        :return The VT sequence
        """
        # The actual VT sequence use coordinates starting from 1
        return VTSequence(f"\x1b[{y+1};{x+1}H")

    @staticmethod
    def view_scroll_up(n: int = 1) -> "VTSequence":
        """
        Create a VT sequence that scrolls the console up
        :param n: The number of lines to scroll the console \
                  up by. If n <= 0, it is assumed to be 1
        <param name="n">
        The number of lines to scroll the console up by.

        </param>
        <returns>The VT sequence</returns>
        """
        return VTSequence(f"\x1b[{n}S")

    @staticmethod
    def view_scroll_down(n: int = 1):
        """
        Create a VT sequence that scrolls the console down
        :param n: The number of lines to scroll the console \
                  down by. If n <= 0, it is assumed to be 1
        <returns>The VT sequence</returns>
        """
        return VTSequence(f"\x1b[{n}T")

    @staticmethod
    def insert_char(n: int = 1):
        """
        Create a VT sequence that inserts
        whitespaces after the current position
        :param n: The number of spaces to insert. \
                  If n &lt;= 0, it is assumed to be 1
        :return The VT sequence
        """
        return VTSequence(f"\x1b[{n}@")

    @staticmethod
    def delete_char(n: int = 1):
        """
        Create a VT sequence that deletes
        characters after the current position
        :param n: The number of characters to insert. \
                  If n <= 0, it is assumed to be 1
        :return The VT sequence
        """
        return VTSequence(f"\x1b[{n}P")

    @staticmethod
    def erase_char(n: int = 1):
        """
        Create a VT sequence that erases
        characters after the current position
        :param n: The number of characters to erase. \
                  If n &lt;= 0, it is assumed to be 1
        :return The VT sequence
        """
        return VTSequence(f"\x1b[{n}X")

    @staticmethod
    def insert_line(n: int = 1):
        """
        Create a VT sequence that inserts
        lines at the current position
        :param n: The number of lines to insert. \
                  If n &lt;= 0, it is assumed to be 1
        :return The VT sequence
        """
        return VTSequence(f"\x1b[{n}L")

    @staticmethod
    def delete_line(n: int = 1):
        """
        Create a VT sequence that deletes
        lines at the current position
        :param n: The number of lines to delete. \
                  If n &lt;= 0, it is assumed to be 1
        :return The VT sequence
        """
        return VTSequence(f"\x1b[{n}M")

    def __init__(self, sequence: str):
        """
        Create a new VT sequence object
        from the given string
        :param sequence: The VT sequence, as a string
        """
        self._sequence = sequence

    def display(self) -> str:
        """
        Get the display representation of this VT sequence, where
        the escape character is replaced with the string "ESC"
        :return: The display representation of this VT sequence
        """
        return self._sequence.replace("\x1b", "ESC")

    def __str__(self) -> str:
        return self._sequence

    def __eq__(self, other: any) -> bool:
        if not isinstance(other, VTSequence):
            return False
        return self._sequence == other._sequence

    def __hash__(self) -> int:
        return hash(self._sequence)
