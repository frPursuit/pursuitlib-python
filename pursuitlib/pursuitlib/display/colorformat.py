class ColorFormat:
    """
    A format used to represent a Color as text

    These formats are compliant with Pursuit's specification:
    https://gitlab.com/frPursuit/wiki/-/blob/master/formats/colors.md
    """

    def __init__(self, name: str, value: int):
        self._name = name
        self._value = value

    def __str__(self) -> str:
        return self._name

    def __int__(self) -> int:
        return self._value

    def __eq__(self, other: any) -> bool:
        if not isinstance(other, ColorFormat):
            return False
        return self._value == other._value

    def __hash__(self) -> int:
        return hash(self._value)


# The hexadecimal ARGB format: #AARRGGBB
HEX_ARGB = ColorFormat("HexArgb", 0x00)

# The ARGB format: argb(A, R, G, B)
ARGB = ColorFormat("Argb", 0x01)

# The AHSV format: ahsv(A, H, S, V)
AHSV = ColorFormat("Ahsv", 0x02)

# The hexadecimal RGB format: #RRGGBB
HEX_RGB = ColorFormat("HexRgb", 0x03)

# The RGB format: rgb(R, G, B)
RGB = ColorFormat("Rgb", 0x04)

# The HSV format: hsv(H, S, V)
HSV = ColorFormat("Hsv", 0x05)
