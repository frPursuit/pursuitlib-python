import math
from mailbox import FormatError
from typing import Tuple, Optional

from pursuitlib.display import colorformat
from pursuitlib.display.colorformat import ColorFormat


class Color:
    """
    A 32 bit ARGB color
    """

    # The luminance threshold used to distinguish
    # light from dark colors
    LUMINANCE_THRESHOLD = 0.179

    @staticmethod
    def parse(value: str) -> "Color":
        """
        Parse a color represented as a string

        Any string compliant with Pursuit's specification is supported:
        https://gitlab.com/frPursuit/wiki/-/blob/master/formats/colors.md
        :param value: The string representing the color
        :return: The color represented by the provided string
        """
        value_lower = value.lower()

        if value_lower.startswith("rgb"):  # RGB representation
            r, g, b = Color._parse_coordinates(int, 3, value, len("rgb"))
            return Color.from_rgb(r, g, b)
        elif value_lower.startswith("argb"):  # ARGB representation
            a, r, g, b = Color._parse_coordinates(int, 4, value, len("argb"))
            return Color.from_argb(a, r, g, b)
        elif value_lower.startswith("hsv"):  # HSV representation
            h, s, v = Color._parse_coordinates(float, 3, value, len("hsv"))
            Color._validate_coordinate('H', h, 0, 360, value)
            Color._validate_coordinate('S', s, 0, 1, value)
            Color._validate_coordinate('V', v, 0, 1, value)
            return Color.from_hsv(h, s, v)
        elif value_lower.startswith("ahsv"):  # AHSV representation
            a, h, s, v = Color._parse_coordinates(float, 4, value, len("hsv"))
            Color._validate_coordinate('A', a, 0.0, 255.0, value)
            Color._validate_coordinate('H', h, 0, 360, value)
            Color._validate_coordinate('S', s, 0, 1, value)
            Color._validate_coordinate('V', v, 0, 1, value)
            return Color.from_ahsv(a, h, s, v)
        elif value_lower.startswith('#'):  # Hexadecimal representation
            digits = value[1:]
            digits_len = len(digits)

            if digits_len == 3:  # #RGB
                r, g, b = digits[0], digits[1], digits[2]
                return Color.from_rgb(int(f"{r}{r}", 16),
                                      int(f"{g}{g}", 16),
                                      int(f"{b}{b}", 16))
            elif digits_len == 4:  # #ARGB
                a, r, g, b = digits[0], digits[1], digits[2], digits[3]
                return Color.from_argb(int(f"{a}{a}", 16),
                                       int(f"{r}{r}", 16),
                                       int(f"{g}{g}", 16),
                                       int(f"{b}{b}", 16))
            elif digits_len == 6:  # #RRGGBB
                return Color.from_rgb(int(digits[0:2], 16),
                                      int(digits[2:4], 16),
                                      int(digits[4:6], 16))
            elif digits_len == 8:  # #AARRGGBB
                return Color.from_argb(int(digits[0:2], 16),
                                       int(digits[2:4], 16),
                                       int(digits[4:6], 16),
                                       int(digits[6:8], 16))
            else:
                raise Color._format_exception(f"Hexadecimal format with {len(digits)} "
                                              f"digits not supported", value)
        else:
            raise Color._format_exception("Unknown format", value)

    @staticmethod
    def from_rgb(r: int, g: int, b: int) -> "Color":
        """
        Get a color from a set of (R, G, B) coordinates
        :param r: The "red" coordinate of the color
        :param g: The "green" coordinate of the color
        :param b: The "blue" coordinate of the color
        :return: The Color instance associated with the provided (R, G, B) coordinates
        """
        return Color.from_argb(255, r, g, b)

    @staticmethod
    def from_hex_rgb(value: int) -> "Color":
        """
        Get a color from an hex-coded RGB color
        :param value: The hex-coded RGB color
        :return: The Color instance associated with the provided hex-coded color
        """
        return Color.from_argb(255,
                               (value >> 16) & 0xFF,
                               (value >> 8) & 0xFF,
                               value & 0xFF)

    @staticmethod
    def from_argb(a: int, r: int, g: int, b: int) -> "Color":
        """
        Get a color from a set of (A, R, G, B) coordinates
        :param a: The "alpha" coordinate of the color
        :param r: The "red" coordinate of the color
        :param g: The "green" coordinate of the color
        :param b: The "blue" coordinate of the color
        :return: The Color instance associated with the provided (A, R, G, B) coordinates
        """
        return Color(a, r, g, b)

    @staticmethod
    def from_hex_argb(value: int) -> "Color":
        """
        Get a color from an hex-coded ARGB color
        :param value: The hex-coded ARGB color
        :return: The Color instance associated with the provided hex-coded color
        """
        return Color.from_argb((value >> 24) & 0xFF,
                               (value >> 16) & 0xFF,
                               (value >> 8) & 0xFF,
                               value & 0xFF)

    @staticmethod
    def from_hsv(h: float, s: float, v: float) -> "Color":
        """
        Get a color from a set of (H, S, V) coordinates
        :param h: The "hue" coordinate of the color
        :param s: The "saturation" coordinate of the color
        :param v: The "value" coordinate of the color
        :return: The Color instance associated with the provided (H, S, V) coordinates
        """
        return Color.from_ahsv(255, h, s, v)

    @staticmethod
    def from_ahsv(a: int, h: float, s: float, v: float) -> "Color":
        """
        Get a color from a set of (A, H, S, V) coordinates
        :param a: The "alpha" coordinate of the color
        :param h: The "hue" coordinate of the color
        :param s: The "saturation" coordinate of the color
        :param v: The "value" coordinate of the color
        :return: The Color instance associated with the provided (A, H, S, V) coordinates
        """
        if a < 0 or a > 255:
            raise ValueError(f"The alpha coordinate must be between 0 and 255 ({a})")
        if h < 0 or h > 360:
            raise ValueError(f"The hue coordinate must be between 0.0 and 360.0 ({h})")
        if s < 0.0 or s > 1.0:
            raise ValueError(f"The saturation coordinate must be between 0.0 and 1.0 ({s})")
        if v < 0.0 or v > 1.0:
            raise ValueError(f"The value coordinate must be between 0.0 and 1.0 ({v})")

        hi = int(math.floor(h / 60)) % 6
        f = h / 60 - math.floor(h / 60)

        v *= 255
        v_int = int(v)
        p = int(v * (1 - s))
        q = int(v * (1 - f * s))
        t = int(v * (1 - (1 - f) * s))

        if hi == 0:
            return Color.from_argb(a, v_int, t, p)
        elif hi == 1:
            return Color.from_argb(a, q, v_int, p)
        elif hi == 2:
            return Color.from_argb(a, p, v_int, t)
        elif hi == 3:
            return Color.from_argb(a, p, q, v_int)
        elif hi == 4:
            return Color.from_argb(a, t, p, v_int)
        else:
            return Color.from_argb(a, v_int, p, q)

    # ----------------------------------------
    # Parsing utility functions
    # ----------------------------------------

    @staticmethod
    def _parse_coordinates(coord_type: type, count: int, value: str, index: int) -> tuple:
        coords = []
        coord_index = 0

        index = Color._skip_spaces(value, index)
        Color._expect(value, index, '(')
        index += 1

        while True:
            if coord_index == count:
                Color._format_exception(f"Too many coordinates found (expected {count})", value)

            index = Color._skip_spaces(value, index)
            sep = value.index(',', index)
            end_sep = value.index(')', index)

            # If both separators were found, get the closest
            if sep != -1 and end_sep != -1:
                sep = min(sep, end_sep)
            elif end_sep != -1:
                sep = end_sep  # If no more commas are present, use ')' as the separator

            if sep == -1:
                Color._format_exception(f"Coordinate separator (',' or ')') not found after index {index}", value)

            coords[coord_index] = coord_type(value[index:sep])
            coord_index += 1

            index = sep

            if value[index] == ',':
                index += 1  # Skip the comma
            else:
                break  # Exit the loop

        Color._expect(value, index, ')')
        index += 1
        Color._expect_end(value, index)

        if coord_index != count:
            raise Color._format_exception(f"Found {coord_index} coordinates, but expected {count}", value)

        return tuple(coords)

    @staticmethod
    def _expect_not_end(value: str, index: int):
        if index == len(value):
            raise Color._format_exception("Unexpected end of string", value)

    @staticmethod
    def _expect_end(value: str, index: int):
        if index < len(value):
            raise Color._format_exception(f"Expected end of string at index {index}, "
                                          f"found {value[index]}", value)

    @staticmethod
    def _skip_spaces(value: str, index: int) -> int:
        while index < len(value) and value[index] == ' ':
            index += 1
        Color._expect_not_end(value, index)
        return index

    @staticmethod
    def _expect(value: str, index: int, character: str):
        if value[index] != character:
            raise Color._format_exception(f"Expected character '{character}' at index {index}, "
                                          f"found {value[index]}", value)

    @staticmethod
    def _validate_coordinate(name: str, value: float, min_val: float, max_val: float, full_value: str):
        if value < min_val or value > max_val:
            raise Color._format_exception(f"Invalid {name} coordinate: {value} is not "
                                          f"between {min_val} and {max_val}", full_value)

    @staticmethod
    def _format_exception(error: str, value: str) -> FormatError:
        return FormatError(f"Invalid color format: {error} ({value})")

    def __init__(self, a: int, r: int, g: int, b: int):
        """
        Create a color instance from the
        specified A, R, G, B values
        :param a: The "alpha" coordinate of the color (that determines transparency)
        :param r: The "red" coordinate of the color
        :param g: The "green" coordinate of the color
        :param b: The "blue" coordinate of the color
        """
        self._a = a
        self._r = r
        self._g = g
        self._b = b

    @property
    def a(self) -> int:
        """
        The "alpha" coordinate of the color (that determines transparency)
        """
        return self._a

    @property
    def r(self) -> int:
        """
        The "red" coordinate of the color
        """
        return self._r

    @property
    def g(self) -> int:
        """
        The "green" coordinate of the color
        """
        return self._g

    @property
    def b(self) -> int:
        """
        The "blue" coordinate of the color
        """
        return self._b

    @property
    def hsv(self) -> Tuple[float, float, float]:
        """
        This color's (H, S, V) coordinates (Hue, Saturation, Value)
        """
        # Get the R,G,B coordinates in a scale from 0 to 1
        r = self.r / 255
        g = self.g / 255
        b = self.b / 255

        max_coord = max(r, g, b)
        min_coord = min(r, g, b)

        if max_coord == r:
            h = (g - b) / (max_coord - min_coord)
        elif max_coord == g:
            h = 2 + (b - r) / (max_coord - min_coord)
        else:  # if max_coord == b
            h = 4 + (r - g) / (max_coord - min_coord)

        s = 0 if max_coord == 0 else 1 - (1 * min_coord / max_coord)
        v = max_coord

        return h, s, v

    @property
    def luminance(self) -> float:
        """
        The relative luminance of this color,
        as defined by the WCAG Working Group:
        https://www.w3.org/WAI/GL/wiki/Relative_luminance
        """
        coords = [self.r / 255, self.g / 255, self.b / 255]
        for i in range(3):
            if coords[i] <= 0.04045:
                coords[i] /= 12.92
            else:
                coords[i] = ((coords[i] + 0.055) / 1.055) ** 2.4
        return 0.2126 * coords[0] + 0.7152 * coords[1] + 0.0722 * coords[2]

    @property
    def is_dark(self) -> bool:
        """
        Check whether this color is characterized as dark.

        Dark colors have a luminance lower than the threshold defined by
        "Color.LUMINANCE_THRESHOLD"
        :return:
        """
        return self.luminance <= Color.LUMINANCE_THRESHOLD

    def to_opaque(self) -> "Color":
        """
        Return an opaque variant of this color,
        for which the A coordinate is set to 255
        :return: The opaque variant of this color, with A = 255
        """
        return Color.from_rgb(self.r, self.g, self.b)

    def to_argb(self) -> int:
        """
        Get the integer ARGB representation of this color
        :return: The integer ARGB representation of this color
        """
        return (self.a << 24) + (self.r << 16) + (self.g << 8) + self.b

    def to_rgb(self) -> int:
        """
        Get the integer RGB representation of this color
        :return: The integer RGB representation of this color
        :raise ValueError: If this color doesn't have an RGB representation
        """
        if self.a != 255:
            raise ValueError(f"This color doesn't have an RGB representation, as its "
                             f"A coordinate is equal to {self.a} (!= 255)")
        return (self.r << 16) + (self.g << 8) + self.b

    def to_string(self, color_format: Optional[ColorFormat] = None) -> str:
        """
        Get the string representation of this color,
        using the specified format
        :param color_format: The format of the color's string representation, \
                             from the colorformat module
        :return: The string representation of the provided color
        :raise ValueError: If the provided color cannot be represented in the specified format
        """
        if color_format is None:
            color_format = colorformat.HEX_RGB if self.a == 255 else colorformat.HEX_ARGB

        if self.a != 255 and color_format in [colorformat.HEX_RGB, colorformat.RGB, colorformat.HSV]:
            raise ValueError(f"Cannot use format {color_format} to represent a color whose alpha "
                             f"coordinate isn't 255 ({self.a})")

        if color_format == colorformat.HEX_ARGB:
            return f"#{self.a:02X}{self.r:02X}{self.g:02X}{self.b:02X}"
        elif color_format == colorformat.ARGB:
            return f"argb({self.a}, {self.r}, {self.g}, {self.b})"
        elif color_format == colorformat.AHSV:
            h, s, v = self.hsv
            return f"ahsv({self.a}, {h}, {s}, {v})"
        elif color_format == colorformat.HEX_RGB:
            return f"#{self.r:02X}{self.g:02X}{self.b:02X}"
        elif color_format == colorformat.RGB:
            return f"rgb({self.r}, {self.g}, {self.b})"
        elif color_format == colorformat.HSV:
            h, s, v = self.hsv
            return f"hsv({h}, {s}, {v})"
        else:
            raise ValueError(f"Unknown color format: {color_format}")

    def __str__(self) -> str:
        return self.to_string()

    def __eq__(self, other: any) -> bool:
        if not isinstance(other, Color):
            return False
        return ((self._a, self._r, self._g, self._b) ==
                (other._a, self._r, self._g, self._b))

    def __hash__(self) -> int:
        return hash((self._a, self._r, self._g, self._b))
