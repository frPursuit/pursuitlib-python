"""
This module contains assorted utility methods
"""

import os
from typing import List


def is_null_or_empty(value) -> bool:
    """
    Check if the provided value can be considered "null" (ie: is None) or "empty" (ie: str(value) == "")
    :param value: The value to check
    :return: True if the provided value is considered null or empty, false otherwise
    """
    return value is None or str(value) == ""


def is_null_or_white_space(value) -> bool:
    """
    Check if the provided value can be considered "null" or equivalent to a set of whitespaces
    :param value: The value to check
    :return: True if the provided value is considered null or whitespaces, false otherwise
    """
    if value is None:
        return True
    return is_null_or_empty(str(value).strip())


def get_oneline_string(string: str) -> str:
    """
    Transform the string to a single-line string by replacing newlines and tabs with whitespaces
    :param string: The string to transform
    :return: The single-line version of the provided string
    """
    return string.replace("\r", "").replace("\n", " ").replace("\t", " ")


def list_to_string(objects: List) -> str:
    """
    Transform a list to a string containing the string representation of all of its elements, separated by commas
    :param objects: The list to transform
    :return: The string representation of the provided string
    """
    return ", ".join(map(str, objects))


def get_env(name: str, default=None) -> str:
    """
    Get an environment variable, or a provided default value.
    If the environment variable doesn't exist and no default value is provided (ie: default=None),
    an EnvironmentError exception is raised
    :param name: The name of the environment variable to get
    :param default: The default value to use when the environment variable doesn't exist
    :return: The value of the environment variable, or "default" when applicable
    """
    value = os.getenv(name, default)
    if value is None:
        raise EnvironmentError(f"The environment variable \"{name}\" is not defined")
    return value


def delete_file(path: str):
    """
    Delete a file if it exists.
    If the provided path points to a directory or a non-existant file, this function does nothing
    :param path: The path to the file to delete
    """
    if os.path.isfile(path):
        os.remove(path)
