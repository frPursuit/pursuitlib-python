"""
This module contains helpful iterators
"""

from typing import TypeVar, Iterable, Iterator, Callable, Optional

T = TypeVar("T")
R = TypeVar("R")


class ChainableIterator(Iterator[T]):
    """
    Iterator that allow the use of transformers using methods
    rather than functions
    """
    def __init__(self, base: Iterator[T]):
        self.base = base

    def first(self) -> T:
        """
        Get the first item provided by the underlying iterator
        :return: The first item provided by the iterator
        """
        return next(self.base)

    def first_or_default(self, default: Optional[T] = None) -> Optional[T]:
        """
        Try to get the first item provided by the underlying iterator
        If none is available, this method returns a default value
        :param default: The default value to return where no element is provided by the iterator
        :return: The first item provided by the iterator, or the "default" parameter
        """
        try:
            return next(self.base)
        except StopIteration:
            return default

    def filter(self, function: Callable[[T], bool]) -> "ChainableIterator[T]":
        """
        Filter the underlying iterator using the provided function
        :param function: A filter function that should return "true" if the provided item passes the filter, and false otherwise
        :return: The filtered ChainableIterator
        """
        iterator = filter(function, self.base)
        return ChainableIterator(iterator)

    def map(self, function: Callable[[T], R]) -> "ChainableIterator[R]":
        """
        Map each item from the underlying iterator to another
        :param function: The mapping function that transforms an item from the iterator to another
        :return: The mapped ChainableIterator
        """
        iterator = map(function, self.base)
        return ChainableIterator(iterator)

    def to_list(self):
        """
        Get a list of all the items provided by the underlying iterator
        :return: The list of all the items provided by the iterator
        """
        return list(self.base)

    def count(self):
        """
        Returns the number of items provided by the underlying iterator
        WARNING: This consumes the iterator, meaning it cannot be reused after this method is called
        :return: The number of items provided by the iterator
        """
        return sum((1 for _ in self.base))

    def __next__(self) -> T:
        return next(self.base)


def citer(iterable: Iterable[T]) -> ChainableIterator[T]:
    """
    Create a ChainableIterator from an iterable object
    :param iterable: The iterable object to create the ChainableIterator from
    :return: A ChainableIterator created from the provided iterable
    """
    return ChainableIterator(iter(iterable))
