"""
This module contains helper methods that can be used to quickly parse values
"""

from typing import Optional

from decimal import Decimal


def parseint(value, default: Optional[int] = None) -> Optional[int]:
    """
    Try to parse an integer
    :param value: The iteger to parse
    :param default: The value that should be returned if the parsing fails
    :return: The parsed integer, or the "default" parameter if the parsing fails
    """
    if value is not None:
        try:
            return int(value)
        except ValueError:
            pass

    return default


def parsefloat(value, default: Optional[float] = None) -> Optional[float]:
    """
    Try to parse a float
    :param value: The float to parse
    :param default: The value that should be returned if the parsing fails
    :return: The parsed float, or the "default" parameter if the parsing fails
    """
    if value is not None:
        try:
            return float(value)
        except ValueError:
            pass

    return default


def parsedecimal(value, default: Optional[Decimal] = None) -> Optional[Decimal]:
    """
    Try to parse a decimal
    :param value: The decimal to parse
    :param default: The value that should be returned if the parsing fails
    :return: The parsed decimal, or the "default" parameter if the parsing fails
    """
    if value is not None:
        try:
            return Decimal(value)
        except ValueError:
            pass

    return default


def parseas(type_name: str, value, default=None):
    """
    Try to parse the provided value as the specified type
    :param type_name: The type to parse the value as
    :param value: The value to parse
    :param default: The value that should be returned if the parsing fails
    :return: The parsed value, or the "default" parameter if the parsing fails
    """
    if value is not None:
        try:
            if type_name == "str":
                return str(value)
            elif type_name == "int":
                return int(value)
            elif type_name == "float":
                return float(value)
            elif type_name == "Decimal":
                return Decimal(value)
        except ValueError:
            pass

    return default
