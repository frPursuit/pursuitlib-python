# PursuitLib for Python

Modular library that provides utility functions and mechanisms for Python and Python-based frameworks.

All modules are available on [PyPi](https://pypi.org/).

## Modules

- `pursuitlib`, the core library
- `pursuitlib-qrcode`, library for generating QR Codes and PDF files
- `pursuitlib-django`, library specific to the [Django](https://www.djangoproject.com/) framework
