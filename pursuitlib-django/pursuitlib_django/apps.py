from django.apps import AppConfig


class PursuitlibConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pursuitlib_django'
